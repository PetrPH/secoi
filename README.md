# SECOI
We introduce SECOI, an application that creates a selective-colored version of an input image according to a user's preference given by reference color. The application is fast, and in comparison with existing approaches, it is resistant against various perturbations in color, and therefore, it is able to include desired pixels with slightly different colors and filter out outliers with the same color as is the reference. According to the benchmark, SECOI yields better Dice, RMSE, and SSIM coefficients than the standard algorithms. The idea of SECOI is based on the theory of fuzzy soft sets where similarity relation is used for their construction. The color filtering is then realized on the proper selection of alpha-cut of the set. We propose SECOI as a global method or as SECOI+dist, where by involving spatial weighting, we create a local method with increased performance.

## Demonstration movie:
[![Demonstration movie](https://img.youtube.com/vi/dlygaNzZ0G8/0.jpg)](https://www.youtube.com/watch?v=dlygaNzZ0G8)

