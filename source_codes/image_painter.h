#ifndef IMAGE_PAINTER_H
#define IMAGE_PAINTER_H

#include <QMainWindow>
#include <QtGui>
#include <QAction>
#include <QCursor>
#include <QMouseEvent>
#include <QLabel>
#include <QPushButton>
#include <QGroupBox>
#include <QProgressBar>
#include <QSlider>
#include <QWidget>
#include <stdlib.h>
#include <iostream>

#include "core_mockor.h"
#include "structs.h"

using namespace std;

class Image_painter : public QLabel{
    Q_OBJECT
    signals:
        void onClick();
        void onMove();

    public:
        Image_painter( QWidget* parent = 0);
        Image_painter( const QString& text, QWidget* parent = 0, Qt::WindowFlags f = 0 );
        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *);
        void mouseReleaseEvent(QMouseEvent *);
        void wheelEvent(QWheelEvent *event);
        void solve_click();
        QImage export_final_output();

    public:
        Image_painter* child = 0;
        QLabel *label_progress;
        QGroupBox *groupBox_progress;
        QProgressBar *progressBar_progress;
        QWidget *centralWidget;
        QSlider *horizontalSlider_thresh;
        bool is_master = false;
        QImage im_orig;
        QImage im_orig_s;
        QImage im_act_s;
        uint *processed;
        bool ready;
        bool right;
        bool hue;
        bool saturation;
        bool value;
        bool soft_color;
        bool use_dist;
        QList<history> all_history;


    private:
        void solve_moving(QPoint pos);

    private:
        QWidget* parent;
        bool clicked;
        float arx;
        float ary;
        QPoint selected;
        uint cursor_type;
        int cursor_radius;

};


#endif // IMAGE_PAINTER_H
