#include "mainwindow.h"
#include "ui_mainwindow.h"

void evaluate_grabcut(){
    for (int i=1; i<=10; i++){
        QImage gc = QImage("dataset/grabcut_out/grabcut_"+QString::number(i)+".tif");
        QImage gt = QImage("dataset/y/"+QString::number(i)+".tif");
        cout<<i<<"\t"<<Metrics::dice(&gc, &gt)<<"\t"<<Metrics::rmse(&gc, &gt)/255.0<<"\t"<<Metrics::ssim(&gc, &gt)<<"\n";
    }
}


void evaluate_psp(){
    for (int i=1; i<=10; i++){
        QImage gc = QImage("dataset/psp_out/psp_"+QString::number(i)+".tif");
        QImage gt = QImage("dataset/y/"+QString::number(i)+".tif");
        cout<<i<<"\t"<<Metrics::dice(&gc, &gt)<<"\t"<<Metrics::rmse(&gc, &gt)/255.0<<"\t"<<Metrics::ssim(&gc, &gt)<<"\n";
    }
}


void evaluate_gimp(){
    for (int i=1; i<=10; i++){
        QImage gc = QImage("dataset/gimp_out/gimp_"+QString::number(i)+".tif");
        QImage gt = QImage("dataset/y/"+QString::number(i)+".tif");
        cout<<i<<"\t"<<Metrics::dice(&gc, &gt)<<"\t"<<Metrics::rmse(&gc, &gt)/255.0<<"\t"<<Metrics::ssim(&gc, &gt)<<"\n";
    }
}


void evaluate(){
    for (int i=1; i<=10; i++){
        QImage gc = QImage("dataset/oursd_out_d/out_oursd_"+QString::number(i)+".png");
        QImage gt = QImage("dataset/y/"+QString::number(i)+".tif");
        cout<<i<<"\t"<<Metrics::dice(&gc, &gt)<<"\t"<<Metrics::rmse(&gc, &gt)/255.0<<"\t"<<Metrics::ssim(&gc, &gt)<<"\n";
    }
}

MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent), ui(new Ui::MainWindow){
    ui->setupUi(this);
    ui->centralWidget->setLayout(ui->gridLayout);
    ui->groupBox_progress->setVisible(false);

    im_orig_label                          = new Image_painter(this);
    im_orig_label->is_master               = true;
    im_orig_label->progressBar_progress    = ui->progressBar_progress;
    im_orig_label->groupBox_progress       = ui->groupBox_progress;
    im_orig_label->centralWidget           = ui->centralWidget;
    //im_orig_label->label_progress          = ui->label_progress;
    im_orig_label->horizontalSlider_thresh = ui->horizontalSlider;

    im_orig_label->setCursor(Qt::CrossCursor);
    im_orig_label->setScaledContents(true);
    //im_orig_label->setText("Waiting for an image");
    ui->horizontalLayout->addWidget(im_orig_label);

    im_mod_label             = new Image_painter(this);
    im_mod_label->is_master  = false;
    im_mod_label->setScaledContents(true);
    //im_mod_label->setText("Waiting for the  selected pixel from the image.");
    ui->horizontalLayout->addWidget(im_mod_label);


    QCursor cursor = QCursor(QPixmap::fromImage(QImage("resources/cursor_circle_32.png")), 15, 15);
    im_mod_label->setCursor(cursor);

    im_orig_label->child     = im_mod_label;
    first_open = true;
    //evaluate_gimp();
    //evaluate();
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::on_pushButton_clicked(){
    this->fileName         = QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp)"));
    im_orig_label->im_orig   = QImage(fileName);
    QRect geom = this->geometry();

    im_mod_label->setPixmap(QPixmap::fromImage(QImage()));
    im_orig_label->setPixmap(QPixmap::fromImage(QImage()));

    if (im_orig_label->im_orig.width() > im_orig_label->im_orig.height()){
        im_orig_label->im_orig_s = im_orig_label->im_orig.scaledToWidth(im_orig_label->width(), Qt::SmoothTransformation);
        im_mod_label->im_orig_s  = im_orig_label->im_orig_s;
        im_orig_label->setPixmap(QPixmap::fromImage(im_orig_label->im_orig_s));
        int diff = (im_orig_label->height())-im_mod_label->im_orig_s.height();
        QRect g = im_orig_label->geometry();
        im_orig_label->setGeometry(g.x(), g.y(), g.width(), g.height()-diff);
        g = im_mod_label->geometry();
        im_mod_label->setGeometry(g.x(), g.y(), g.width(), g.height()-diff);
        this->setGeometry(geom.x(), geom.y(), geom.width(), geom.height()-diff);
    }
    else{
        im_orig_label->im_orig_s = im_orig_label->im_orig.scaledToHeight(im_orig_label->height(), Qt::SmoothTransformation);
        im_mod_label->im_orig_s  = im_orig_label->im_orig_s;
        im_orig_label->setPixmap(QPixmap::fromImage(im_orig_label->im_orig_s));
        int diff = (im_orig_label->width())-im_mod_label->im_orig_s.width();
        QRect g = im_orig_label->geometry();
        im_orig_label->setGeometry(g.x(), g.y(), g.width()-diff, g.height());
        g = im_mod_label->geometry();
        im_mod_label->setGeometry(g.x(), g.y(), g.width()-diff, g.height());
        this->setGeometry(geom.x(), geom.y(), geom.width()-2*diff, geom.height());
    }
    im_mod_label->all_history.clear();
}


void MainWindow::on_horizontalSlider_valueChanged(int value){
    if (!im_orig_label->ready) return;
    im_mod_label->im_act_s = Core_mockor::compose_image(im_orig_label->im_orig_s, im_orig_label->processed, value, im_orig_label->soft_color);
    im_mod_label->setPixmap(QPixmap::fromImage(im_mod_label->im_act_s));
    im_mod_label->all_history.clear();
}


void MainWindow::on_checkBox_hue_toggled(bool checked){
    im_orig_label->hue = checked;
    im_orig_label->solve_click();
}

void MainWindow::on_checkBox_saturation_toggled(bool checked){
    im_orig_label->saturation = checked;
    im_orig_label->solve_click();
}

void MainWindow::on_checkBox_value_toggled(bool checked){
    im_orig_label->value = checked;
    im_orig_label->solve_click();
}


void MainWindow::on_checkBox_soft_colors_toggled(bool checked){
    im_orig_label->soft_color = checked;
    //im_mod_label->im_act_s = Core_mockor::compose_image(im_orig_label->im_orig_s, im_orig_label->processed, ui->horizontalSlider->value(), checked);
    //im_mod_label->setPixmap(QPixmap::fromImage(im_mod_label->im_act_s));
    im_orig_label->solve_click();
}

void MainWindow::on_checkBox_use_dist_toggled(bool checked){
    im_orig_label->use_dist = checked;
    im_orig_label->solve_click();
}



//export output
void MainWindow::on_pushButton_2_clicked(){
    QImage out = im_orig_label->export_final_output();
    out.save("out.png", "png");
    cout<<"exporting\n";
    cout<<this->fileName.toStdString().c_str()<<"\n";

    if (!this->fileName.contains("/dataset/x")) return;
    QString new_name = QString(this->fileName);
    cout<<new_name.toStdString().c_str()<<"\n";
    new_name = new_name.replace("/dataset/x", "/dataset/y").replace("jpg", "tif");
    QImage gt = QImage(new_name);
    cout<<"dice: "<<Metrics::dice(&out, &gt)<<"\n";
    cout<<"rmse: "<<Metrics::rmse(&out, &gt)/255.0<<"\n";
    cout<<"ssim: "<<Metrics::ssim(&out, &gt)<<"\n";
    cout<<"\n";
    //run evaluation

}


