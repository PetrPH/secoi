#ifndef METRICS_H
#define METRICS_H

#include <QImage>

class Metrics{
    public:
        static float dice(QImage *im1, QImage *im2);
        static float rmse(QImage *im1, QImage *im2);
        static float ssim(QImage *original, QImage *current);
};

#endif // METRICS_H
